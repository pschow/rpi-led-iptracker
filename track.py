#!/usr/bin/env python


# Modified from ping.py
# http://www.g-loaded.eu/2009/10/30/python-ping/


### Mostly original ping.py code

import os, sys, socket, struct, select, time
from time import strftime

import RPi.GPIO as GPIO #import GPIO

GPIO.setmode(GPIO.BCM) #set mode BCM
GPIO.setup(11, GPIO.OUT) #Use pin 11
GPIO.setup(9, GPIO.OUT) #Use pin 9
GPIO.setup(8, GPIO.OUT) #Use pin 8
GPIO.setup(25, GPIO.OUT) #Use pin 25
 
# From /usr/include/linux/icmp.h; your milage may vary.
ICMP_ECHO_REQUEST = 8 # Seems to be the same on Solaris.

log = open('track1.txt', 'w') #open a text file for logging
print log #print what the log file is
log.write('Time,IP,Ping\n') #write to log
 
 
def checksum(source_string):
    """
    I'm not too confident that this is right but testing seems
    to suggest that it gives the same answers as in_cksum in ping.c
    """
    sum = 0
    countTo = (len(source_string)/2)*2
    count = 0
    while count<countTo:
        thisVal = ord(source_string[count + 1])*256 + ord(source_string[count])
        sum = sum + thisVal
        sum = sum & 0xffffffff # Necessary?
        count = count + 2
 
    if countTo<len(source_string):
        sum = sum + ord(source_string[len(source_string) - 1])
        sum = sum & 0xffffffff # Necessary?
 
    sum = (sum >> 16)  +  (sum & 0xffff)
    sum = sum + (sum >> 16)
    answer = ~sum
    answer = answer & 0xffff
 
    # Swap bytes. Bugger me if I know why.
    answer = answer >> 8 | (answer << 8 & 0xff00)
 
    return answer
 
 
def receive_one_ping(my_socket, ID, timeout):
    """
    receive the ping from the socket.
    """
    timeLeft = timeout
    while True:
        startedSelect = time.time()
        whatReady = select.select([my_socket], [], [], timeLeft)
        howLongInSelect = (time.time() - startedSelect)
        if whatReady[0] == []: # Timeout
            return
 
        timeReceived = time.time()
        recPacket, addr = my_socket.recvfrom(1024)
        icmpHeader = recPacket[20:28]
        type, code, checksum, packetID, sequence = struct.unpack(
            "bbHHh", icmpHeader
        )
        if packetID == ID:
            bytesInDouble = struct.calcsize("d")
            timeSent = struct.unpack("d", recPacket[28:28 + bytesInDouble])[0]
            return timeReceived - timeSent
 
        timeLeft = timeLeft - howLongInSelect
        if timeLeft <= 0:
            return
 
 
def send_one_ping(my_socket, dest_addr, ID):
    """
    Send one ping to the given >dest_addr<.
    """
    dest_addr  =  socket.gethostbyname(dest_addr)
 
    # Header is type (8), code (8), checksum (16), id (16), sequence (16)
    my_checksum = 0
 
    # Make a dummy heder with a 0 checksum.
    header = struct.pack("bbHHh", ICMP_ECHO_REQUEST, 0, my_checksum, ID, 1)
    bytesInDouble = struct.calcsize("d")
    data = (192 - bytesInDouble) * "Q"
    data = struct.pack("d", time.time()) + data
 
    # Calculate the checksum on the data and the dummy header.
    my_checksum = checksum(header + data)
 
    # Now that we have the right checksum, we put that in. It's just easier
    # to make up a new header than to stuff it into the dummy.
    header = struct.pack(
        "bbHHh", ICMP_ECHO_REQUEST, 0, socket.htons(my_checksum), ID, 1
    )
    packet = header + data
    my_socket.sendto(packet, (dest_addr, 1)) # Don't know about the 1
 
 
def do_one(dest_addr, timeout):
    """
    Returns either the delay (in seconds) or none on timeout.
    """
    icmp = socket.getprotobyname("icmp")
    try:
        my_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, icmp)
    except socket.error, (errno, msg):
        if errno == 1:
            # Operation not permitted
            msg = msg + (
                " - Note that ICMP messages can only be sent from processes"
                " running as root."
            )
            raise socket.error(msg)
        raise # raise the original error
 
    my_ID = os.getpid() & 0xFFFF
 
    send_one_ping(my_socket, dest_addr, my_ID)
    delay = receive_one_ping(my_socket, my_ID, timeout)
 
    my_socket.close()
    return delay
    
### Main modifications start here
 
 
def verbose_ping(dest_addr, pin, timeout = 2, count = 1):
    """
    Send >count< ping to >dest_addr< with the given >timeout< and display
    the result.
    """
    for i in xrange(count):
        print "ping %s..." % dest_addr,
        try:
            delay  =  do_one(dest_addr, timeout)
        except socket.gaierror, e:
            print "failed. (socket error: '%s')" % e[1]
            log.write('%s,socketerr\n' % ((strftime("%H:%M"))))
            #Turn the LED off if there is an error
            led_off(pin)
            break
 
        if delay  ==  None:
            print "failed. (timeout within %ssec.)" % timeout
            log.write('%s,%s,timeout\n' % (strftime("%H:%M"), dest_addr)) #write to log
            #Turn the LED off it there is a timeout
            led_off(pin)
        else:
            delay  =  delay * 1000
            print "get ping in %0.4fms" % delay
            log.write('%s,%s,%0.4f\n' % (strftime("%H:%M"), dest_addr, delay)) #write to log
            #Turn the LED ON if there is a reply
            led_on(pin)
    print
 
#Function to turn the LED on
def led_on(pin):
	GPIO.output(pin,GPIO.HIGH)
	print "LED %d ON" % (pin)

#Fuction to turn the LED off
def led_off(pin):
	GPIO.output(pin,GPIO.LOW)
	print "LED %d OFF" % (pin)

#Main loop
if __name__ == '__main__':
    counter = 0
    #Loop for awhile
    while counter < 14400:
    	verbose_ping("192.168.1.5",11) #W
    	verbose_ping("192.168.1.24",9) #T
    	verbose_ping("192.168.1.12",8) #L
    	verbose_ping("192.168.1.26",25) #P
    	counter = counter + 1
    #wait 10 seconds
 	time.sleep(10)
 	#verbose_ping("208.67.222.222")
 	#verbose_ping("google.com")
    #verbose_ping("192.168.1.50")
    #verbose_ping("192.168.1.1")
